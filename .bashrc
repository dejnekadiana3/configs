#!/bin/sh

# exports
export BASH_SILENCE_DEPRECATION_WARNING=1
export PATH="${HOME}/.bin:${HOME}/.local/bin:"
export PATH="${PATH}/usr/local/bin:/sbin:/usr/bin:/usr/sbin:/bin:"
export PATH="${PATH}/usr/local/sbin:/opt/bin:/usr/bin/core_perl:/usr/games/bin:"
export PATH="${PATH}/opt/local/bin:/opt/local/sbin:"
export LANG="en_US.UTF-8"
export LD_PRELOAD=""
export EDITOR="kak"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"

# XDG
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
export VIMINIT='let $MYVIMRC="$XDG_CONFIG_HOME/vim/vimrc" | source $MYVIMRC'
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass/passwords
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history
mkdir -p "$XDG_DATA_HOME"/wineprefixes
export WINEPREFIX="$XDG_DATA_HOME"/wineprefixes/default
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export HISTFILE="$XDG_DATA_HOME"/bash/history
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export GOPATH="$XDG_DATA_HOME"/go

mkdir -p "$XDG_CACHE_HOME"/less
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history

#status bar
export STATUS_BAR_FIFO="$XDG_RUNTIME_DIR/status-bar/status-bar-fifo"
mkdir -p "$(dirname $STATUS_BAR_FIFO)"
[ ! -e "$STATUS_BAR_FIFO" ] && mkfifo "$STATUS_BAR_FIFO"

# history
export HISTSIZE=5000
export HISTFIZESIZE=5000
export HISTCONTROL=ignoreboth:erasedups

# bash line
function git_status {
  git_dirty=$(git rev-parse 2>/dev/null && (git diff --no-ext-diff --quiet --exit-code 2> /dev/null || echo -e \•))
  git_dirty_staged=$(git rev-parse 2>/dev/null && (git diff --cached --no-ext-diff --quiet --exit-code 2> /dev/null || echo -e \•))
  git_branch=$(git symbolic-ref --short HEAD 2>/dev/null | cut -c -15)
  if ! [ "$git_branch" == "" ]; then
    echo -e "\001\033[01;35m\002$git_branch\001\033[00m\002\001\033[01;31m\002$git_dirty\001\033[00m\002\001\033[01;32m\002$git_dirty_staged\001\033[00m\002 "
  fi
}
export PS1="\$(git_status)\[\033[01;34m\]\w \[\033[00m\]"

# search
#  export FZF_DEFAULT_COMMAND="$AG -g '' --vimgrep"

# less
export LESSOPEN='| /usr/bin/source-highlight-esc.sh %s'
export LESS='-R '

# alias
alias g="LAST_HISTORY_INDEX=\$HISTCMD . fgit"
alias fh="LAST_HISTORY_INDEX=\$HISTCMD . fh"
alias k="kak"
alias f=". fsr f"
alias d=". fsr d"
alias s=". fsr s"
alias r=". fsr r"
alias ,f=". fsr f ho"
alias ,e="vifm"

if [[ "$OSTYPE" == "darwin"* ]]; then
  alias ls="gls --color"
  alias vim="mvim -v"
else
  alias ls="ls --color"
fi

# vi mode in bash
set -o vi

bind '"\C-p":history-search-backward'
bind '"\C-n":history-search-forward'

# file manager
if [ -n "$INSIDE_VIFM" ]; then export PS1="\[\033[01;33m\][vifm] \[\033[00m\] $PS1"; fi
vifm() {
  if [ -z "$INSIDE_VIFM" ]; then
    $(which vifm) "$@"
  else
    exit
  fi
}

if [ -n "$RANGER_LEVEL" ]; then export PS1="\[\033[01;33m\][ranger] \[\033[00m\] $PS1"; fi
ranger() {
  if [ -z "$RANGER_LEVEL" ]; then
    $(which ranger) "$@"
  else
    exit
  fi
}

# autocompletion
if [[ "$OSTYPE" == "darwin"* ]]; then
  if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
  fi
else
  if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  fi
fi

source $XDG_CONFIG_HOME/broot/launcher/bash/br

ask_to_run_startx () {
  answer="$(echo -e "yes\nno" | dim menu 'printf "Do you want to run startx?"' | awk 'NR==2')"

  [[ "$answer" == "yes" ]] && jdm start
  [[ "$answer" == "no" ]] && clear
}

if [ "$TERM" = "linux" ]; then
  echo -en "\e]P0101010" #black
  echo -en "\e]P82B2B2B" #darkgrey
  echo -en "\e]P1D75F5F" #darkred
  echo -en "\e]P9E33636" #red
  echo -en "\e]P287AF5F" #darkgreen
  echo -en "\e]PA98E34D" #green
  echo -en "\e]P3D7AF87" #brown
  echo -en "\e]PBFFD75F" #yellow
  echo -en "\e]P48787AF" #darkblue
  echo -en "\e]PC7373C9" #blue
  echo -en "\e]P5BD53A5" #darkmagenta
  echo -en "\e]PDD633B2" #magenta
  echo -en "\e]P65FAFAF" #darkcyan
  echo -en "\e]PE44C9C9" #cyan
  echo -en "\e]P7E5E5E5" #lightgrey
  echo -en "\e]PFFFFFFF" #white
  clear #for background artifacting

  ask_to_run_startx
fi

[[ ! -n "$TMUX" ]] && sh $XDG_DATA_HOME/scripts/tmux/tmux-tmp.sh
