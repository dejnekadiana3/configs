#!/bin/sh

dunst &
sxhkd &

picom --experimental-backends &

# sh $XDG_DATA_HOME/scripts/status-bars/polybar.sh &
sh $XDG_DATA_HOME/scripts/status-bars/lemonbar.sh &
# xdo below -t $(xdo id -n root) $(xdo id -a 'lemonbar')

stalonetray --config $XDG_CONFIG_HOME/stalonetray/stalonetrayrc &

flameshot &
blueman-applet &
nm-applet &
