#!/bin/bash

min_brightness=0
max_brightness=1

dim_screen () {
  set_brightness $min_brightness;
}

undim_screen () {
  set_brightness $max_brightness;
}

set_brightness() {
  for monitor_name in $(sh $XDG_DATA_HOME/scripts/monitors.sh names); do
    xrandr --output $monitor_name --brightness $1;
  done
}

trap 'exit 0' TERM INT
trap "undim_screen; kill %%" EXIT
dim_screen
sleep 2147483647 &
wait
