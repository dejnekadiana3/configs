#!/bin/sh

info_all() {
  if [ -z $ALL_MONITORS ]; then
    ALL_MONITORS="$(xrandr | awk '/connected/ {print $1}')"
  fi

  printf "%s\n" "$ALL_MONITORS"
}

info_connected() {
  if [ -z $MONITORS ]; then
    MONITORS="$(xrandr | awk '/\sconnected/ {print $1}')"
  fi

  printf "%s\n" "$MONITORS"
}

info () {
  if [ -z $MONITORS ]; then
    MONITORS="$(xrandr --listmonitors | awk '/^ / {print $0}')"
  fi

  printf "%s\n" "$MONITORS"
}

names () {
  info | awk '{print $4}'
}

dimentions () {
  info | awk '{print $3}'
}

case "$1" in
  "dimentions") dimentions;;
  "names") names;;
  "all") info_all;;
  "connected") info_connected;;
  *) info;;
esac
