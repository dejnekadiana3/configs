#!/bin/sh

updates=$(yay -Qu 2> /dev/null | wc -l)

if [ "$updates" -gt 0 ]; then
    echo "A: $updates"
else
    echo "A: 0"
fi
