#!/bin/sh

nordvpn_status="$(nordvpn status | grep Status | cut -f 2 -d ' ')"
nordvpn_server="$(nordvpn status | grep "Current server" | cut -f 3 -d ' ')"

[[ ! -n "$nordvpn_status" ]] && echo -e "Nordvpn: error" && exit

if [[ "$1" == "connect" ]]; then
  [[ "$nordvpn_status" == "Disconnected" ]] && nordvpn connect $2
  [[ "$nordvpn_status" == "Disconnecting" ]] && nordvpn connect $2
  exit
fi

if [[ "$1" == "disconnect" ]]; then
  [[ "$nordvpn_status" == "Connected" ]] && nordvpn disconnect
  [[ "$nordvpn_status" == "Connecting" ]] && nordvpn disconnect
  exit
fi

printf "%s" "Nordvpn: $([[ -n $nordvpn_server ]] && printf "%s" "$nordvpn_server" || printf "%s" "$nordvpn_status")"

