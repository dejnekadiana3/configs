#!/bin/sh

prepend_zero () {
    seq -f "%02g" $1 $1
}

cmus_status=$(cmus-remote -C status)
song=$(echo -n $(echo "$cmus_status" | grep file -m 1| cut -c 5- | sed 's@'"$HOME/Music/"'@@g' | sed 's@'"$HOME/"'@@g'))

playing=$(echo "$cmus_status" | grep "status playing")

position=$(echo "$cmus_status" | grep position | cut -c 10-)
[[ ! -n "$position" ]] && exit

minutes1=$(prepend_zero $(($position / 60)))
seconds1=$(prepend_zero $(($position % 60)))

duration=$(echo "$cmus_status" | grep duration | cut -c 10-)
[[ ! -n "$duration" ]] && exit

minutes2=$(prepend_zero $(($duration / 60)))
seconds2=$(prepend_zero $(($duration % 60)))

echo -n "$([[ -n $playing ]] && echo "" || echo "") $minutes1:$seconds1 / $minutes2:$seconds2 ${song: -30}"
