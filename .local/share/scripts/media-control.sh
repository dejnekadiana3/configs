#!/bin/sh

action="$1"

cmus-remote -Q &> /dev/null
if [[ "$?" == "0" ]]; then
  [[ "$action" == "play-pause" ]] && \
    cmus-remote -u

  [[ "$action" == "next" ]] && \
    cmus-remote -n

  [[ "$action" == "previous" ]] && \
    cmus-remote -r

  exit
fi

ps -C spotify -o pid= &> /dev/null
if [[ "$?" == "0" ]]; then
  [[ "$action" == "play-pause" ]] && \
    dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause

  [[ "$action" == "next" ]] && \
    dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next

  [[ "$action" == "previous" ]] && \
    dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous

  exit
fi
