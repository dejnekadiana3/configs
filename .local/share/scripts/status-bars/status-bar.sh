#!/bin/sh

repeat () {
  local slogan="$1"
  local cmd="$2"
  local repeat="$3"

  while true; do
    local output="$(bash -c "$cmd")"
    
    pn "$slogan$output"

    sleep $repeat
  done
}

p () {
  printf "%s" "$1"
}

pn () {
  printf "%s\n" "$1"
}

process_bspwm () {
  local status="$1"
  local monitors_status=""

  monitors=( $(p "$status" | sed 's/\:\([mM]\)/ \1/g') )

  for m in "${monitors[@]}"; do
    local m_name="$(p "$m" | cut -d ':' -f 1 | cut -c 2-)"
    local m_focus="$(p "$m" | cut -d ':' -f 1 | cut -c 1)"

    m_status=( $(p "$m" | cut -d ':' -f 2- | sed 's/\:\([LTG]\)/ \1/') )
    desktops=( $(p "${m_status[0]}" | sed 's/\:/ /g') )
    windows_statuses=( $(p "${m_status[1]}" | sed 's/\:/ /g') )

    monitors_status+="$m_focus: "

    for d in "${desktops[@]}"; do
      case $d in
        [o]*) monitors_status+="${d:1} ";;
        [OF]*) monitors_status+="${d:1}$([[ "$m_focus" == "M" ]] && p "*" || p "-") ";;
        [uU]*) monitors_status+="$d ";;
      esac
    done

    local state=""
    local layout=""
    local flags=""

    for w in "${windows_statuses[@]}"; do
      case $w in
        [T]*) state="${w:1}";;
        [L]*) layout="${w:1}";;
        [G]*) flags="${w}";;
      esac
    done

    monitors_status+="[$state,$layout,$flags] ";
  done

  p "${monitors_status:0: -1}"
}

# TODO: fix
xtitle -sf "TITLE%s\n" > "$STATUS_BAR_FIFO" &

repeat 'SPOTIFY' 'python $XDG_DATA_HOME/scripts/statuses/spotify.py' 5 > "$STATUS_BAR_FIFO" &
repeat 'CMUS' 'sh $XDG_DATA_HOME/scripts/statuses/cmus.sh' 5 > "$STATUS_BAR_FIFO" &
repeat 'DATE' "date +'%a %b %d %H:%M'" 60 > "$STATUS_BAR_FIFO" &
# repeat 'NORDVPN' 'sh $XDG_DATA_HOME/scripts/statuses/nordvpn.sh' 30 > "$STATUS_BAR_FIFO" &
repeat 'PAVOLUME' 'sh $XDG_DATA_HOME/scripts/statuses/pavolume.sh' 0.5 > "$STATUS_BAR_FIFO" &
repeat 'BATTERY' 'sh $XDG_DATA_HOME/scripts/statuses/battery.sh' 60 > "$STATUS_BAR_FIFO" &
repeat 'ARCH_UPDATES' 'sh $XDG_DATA_HOME/scripts/statuses/arch-updates.sh' 3600 > "$STATUS_BAR_FIFO" &
repeat 'KEYBOARD_LAYOUT' 'sh $XDG_DATA_HOME/scripts/statuses/keyboard-layout.sh' 0.5 > "$STATUS_BAR_FIFO" &

while read -r line ; do
	case $line in
    W*) BSPWM="$(process_bspwm "$(p "$line" | cut -c 2-)")";;
    TITLE*) TITLE="$(p "$line" | cut -c 6-)";;
    DATE*) DATE="$(p "$line" | cut -c 5-)";;
    CMUS*) CMUS="$(p "$line" | cut -c 5-)";;
    SPOTIFY*) SPOTIFY="$(p "$line" | cut -c 8-)";;
    NORDVPN*) NORDVPN="$(p "$line" | cut -c 8-)";;
    PAVOLUME*) PAVOLUME="$(p "$line" | cut -c 9-)";;
    BATTERY*) BATTERY="$(p "$line" | cut -c 8-)";;
    ARCH_UPDATES*) ARCH_UPDATES="$(p "$line" | cut -c 13-)";;
    KEYBOARD_LAYOUT*) KEYBOARD_LAYOUT="$(p "$line" | cut -c 16-)";;
    *) pn "Error: $line";;
	esac

  statuses_left=(
    "$BSPWM"
    "$TITLE"
  )
  statuses_right=(
    "$SPOTIFY"
    "$CMUS"
    "$NORDVPN"
    "$PAVOLUME"
    "$BATTERY"
    "$ARCH_UPDATES"
    "$KEYBOARD_LAYOUT"
    "$DATE"
  )
  statuses_left_output=""
  statuses_right_output=""

  delimiter=', '

  for status in "${statuses_left[@]}"; do
    [[ -n "$status" ]] && statuses_left_output+="$status$delimiter"
  done

  for status in "${statuses_right[@]}"; do
    [[ -n "$status" ]] && statuses_right_output+="$status$delimiter"
  done

  l="$([[ -n "$statuses_left_output" ]] && p "${statuses_left_output:0: -2}")"
  r="$([[ -n "$statuses_right_output" ]] && p "${statuses_right_output:0: -2}")"
  printf "%s\n" "%{Sf}%{l}$l%{r}$r"
  # printf "%s\n" "%{l}${statuses_left_output:0: -5}%{r}${statuses_right_output:0: -5}"

done < "$STATUS_BAR_FIFO"

wait
