#!/bin/sh

STATUS_BAR_WIDTH="$(printf "%s" "$(sh $XDG_DATA_HOME/scripts/monitors.sh dimentions)" | awk -F '[x/+]' 'NR==1 {print $1}')"
STATUS_BAR_HEIGHT="14"

sh $XDG_DATA_HOME/scripts/status-bars/status-bar.sh \
  | dzen2 -p -fn "Font Awesome;UbuntuMono Nerd Font" \
    -w "$(( STATUS_BAR_WIDTH - 7 ))"
    -h "$STATUS_BAR_HEIGHT" \
    -x "3" \
    -y "3"
