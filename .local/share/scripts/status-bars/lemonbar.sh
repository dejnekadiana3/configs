#!/bin/sh

STATUS_BAR_WIDTH="$(printf "%s" "$(sh $XDG_DATA_HOME/scripts/monitors.sh dimentions)" | awk -F '[x/+]' 'NR==1 {print $1}')"
STATUS_BAR_HEIGHT="14"

sh $XDG_DATA_HOME/scripts/status-bars/status-bar.sh \
  | lemonbar -n "lemonbar" \
    -F '#e8e8e2' \
    -o -1 \
    -f "Noto Mono" -f "Font Awesome" \
    -g "$(( STATUS_BAR_WIDTH - 7 - 70 ))x$STATUS_BAR_HEIGHT+3+3"
