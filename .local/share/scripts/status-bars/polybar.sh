#!/bin/sh

monitor="$(printf "%s" "$(sh $XDG_DATA_HOME/scripts/monitors.sh dimentions)" | awk '{print $1}')"

case "$(sh $XDG_DATA_HOME/scripts/get-currect-wm.sh)" in
  "bspwm") MONITOR=$monitor polybar --reload mainbar-bspwm;;
esac

