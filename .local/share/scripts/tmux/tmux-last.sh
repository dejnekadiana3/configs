#!/bin/sh

tmp_dir=/tmp/$USER
mkdir -p $tmp_dir

tmux_last() {
  local last_session
  local last_window

  [ ! -f "$tmp_dir/TMUX_LAST_WINDOW" ] && return;

  last_session="$(cut -f 1 -d ':' "$tmp_dir/TMUX_LAST_WINDOW")"
  last_window="$(cut -f 2 -d ':' "$tmp_dir/TMUX_LAST_WINDOW")"
  
  [[ -n "$last_session" ]] || exit
  [[ -n "$last_window" ]] || exit

  tmux display-message -p "#{session_name}:#{window_id}" > "$tmp_dir/TMUX_LAST_WINDOW"

  if [[ -n "$TMUX" ]]; then
    tmux switch-client -t "$last_session"
    tmux select-window -t "$last_window"
  else
    tmux attach-session -t "$last_session"
    tmux select-window -t "$last_window"
  fi
}

tmux_last
