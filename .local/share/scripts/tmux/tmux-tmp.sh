#!/bin/sh

tmux_tmp() {
  local tmp_sessions="$(tmux list-sessions -F '#{session_name} #{session_attached}' | awk '/^tmp/')"
  local session="tmp$(( RANDOM % 10000 ))"

  printf "%s" "$tmp_sessions" | awk '/0$/ {print $1}' | xargs -r -I "()" tmux kill-session -t "()"

  if [[ -n "$TMUX" ]]; then
    tmux switch-client -t "$session"
  else
    tmux new-session -s "$session"
  fi
}

tmux_tmp
