#!/bin/sh

if [[ "$OSTYPE" == "darwin"* ]]; then
  CURR_DIR="$( dirname $(greadlink -f $0) )"
else
  CURR_DIR="$( dirname $(readlink -f $0) )"
fi

cd $CURR_DIR/../../../../../

echo "Dotfiles directory: $(pwd)/config"

stow -t ~ --no-folding configs

echo "Finished"
