#!/bin/sh

if [[ "$OSTYPE" == "darwin"* ]]; then
  CURR_DIR="$( dirname $(greadlink -f $0) )"
else
  CURR_DIR="$( dirname $(readlink -f $0) )"
fi

cd $CURR_DIR

echo "Linking:"

function linker {
  if [ -z "$1" ]; then exit; fi

  local path="$1"
  local parent=$(echo "$path" | rev | cut -s -f2- -d"/" | rev)
  local other_path2="$2"
  local other_parent2=$(echo "$other_path2" | rev | cut -s -f2- -d"/" | rev)
  local other_path3="$3"
  local other_parent3=$(echo "$other_path3" | rev | cut -s -f2- -d"/" | rev)

  if [ -d "$HOME/$parent" ] && ! [ -z "$path" ]; then
    rm -rf "$HOME/$path"
    ln -sf "$CURR_DIR/../$path" "$HOME/$path"
    printf "%s" "$path, "

    if [ -d "$HOME/$other_parent2" ] && ! [ -z "$other_path2" ]; then
      rm -rf "$HOME/$other_path2"
      ln -sf "$CURR_DIR/../$path" "$HOME/$other_path2"
      printf "%s" "$other_path2, "
    fi

    if [ -d "$HOME/$other_parent3" ] && ! [ -z "$other_path3" ]; then
      rm -rf "$HOME/$other_path3"
      ln -sf "$CURR_DIR/../$path" "$HOME/$other_path3"
      printf "%s" "$other_path3, "
    fi
  fi
}

printf "%s" "- linking "
linker .emacs
linker .bashrc
linker .bash_profile
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.local"
linker .local/bin
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.local/share"
linker .local/share/wallpapers
linker .local/share/scripts
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.local/share/applications"
linker .local/share/applications/qutebrowser.desktop
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.local/share/autoload.d"
linker .local/share/autoload.d/startup.sh
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.local/share/gnupg"
linker .local/share/gnupg/gpg-agent.conf
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.w3m"
linker .w3m/keymap
linker .w3m/config
linker .w3m/bookmark.html
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.config"
if [[ "$OSTYPE" == "darwin"* ]]; then mkdir -p "$HOME/Library/Preferences"; fi
linker .config/mimeapps.list
linker .config/ranger
linker .config/alacritty
linker .config/i3
linker .config/picom
linker .config/i3status
linker .config/polybar
linker .config/dim
linker .config/bspwm
linker .config/sxhkd
linker .config/dunst
linker .config/Dharkael
linker .config/awesome
linker .config/readline
linker .config/vim
linker .config/kak
linker .config/X11
linker .config/stalonetray
linker .config/broot Library/Preferences/org.dystroy.broot
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.config/tmux"
linker .config/tmux/tmux.conf
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.config/vifm"
linker .config/vifm/vifmrc
linker .config/vifm/colors
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.config/cmus"
linker .config/cmus/rc
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.config/vimb"
linker .config/vimb/config
linker .config/vimb/style.css
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.config/rofi"
linker .config/rofi/config
linker .config/rofi/theme
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.config/qutebrowser"
if [[ "$OSTYPE" == "darwin"* ]]; then mkdir -p "$HOME/Library/Preferences/qutebrowser"; fi
linker .config/qutebrowser/autoconfig.yml Library/Preferences/qutebrowser/autoconfig.yml
linker .config/qutebrowser/bookmarks Library/Preferences/qutebrowser/bookmarks
linker .config/qutebrowser/quickmarks Library/Preferences/qutebrowser/quickmarks
linker .config/qutebrowser/config.py Library/Preferences/qutebrowser/config.py
linker .config/qutebrowser/draculaDraw.py Library/Preferences/qutebrowser/draculaDraw.py
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.config/lutris"
linker .config/lutris/games
linker .config/lutris/runners
echo ""

printf "%s" "- linking "
mkdir -p "$HOME/.config/aerc"
linker .config/aerc/binds.conf
linker .config/aerc/aerc.conf
echo ""

echo "Finished"
