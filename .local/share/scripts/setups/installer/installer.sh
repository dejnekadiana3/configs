#!/bin/sh

if [[ "$OSTYPE" == "darwin"* ]]; then
  CURR_DIR="$( dirname $(greadlink -f $0) )"
else
  CURR_DIR="$( dirname $(readlink -f $0) )"
fi

cd $CURR_DIR

cat packages-list.txt | awk '!/(^#)|(^$)/' | xargs -r -o -n 10000 sudo pacman -S --needed

if ! [ -x "$(command -v yay)" ]; then
  cd /tmp
  git clone https://aur.archlinux.org/yay.git
  cd yay
  makepkg -si
  cd $CURR_DIR
fi

cat packages-list-aur.txt | awk '!/(^#)|(^$)/' | xargs -r -o -n 10000 yay -S --needed

# dim
go get github.com/oem/lnch
