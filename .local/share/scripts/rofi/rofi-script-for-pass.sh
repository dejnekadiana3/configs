#!/usr/bin/env bash

if [[ "$@" == "" ]]; then
  shopt -s nullglob globstar


  prefix=${PASSWORD_STORE_DIR-~/.password-store}
  password_files=( "$prefix"/**/*.gpg )
  password_files=( "${password_files[@]#"$prefix"/}" )
  password_files=( "${password_files[@]%.gpg}" )

  echo "$(printf '%s\n' "${password_files[@]}")"
else
  password="$@"

  [[ -n $password ]] || exit

  pass show -c "$password" &>/dev/null &
fi
