#!/usr/bin/env bash

if [[ "$@" == "" ]]; then
  hints=(
    "!gtuk"
    "!yt"
    "!g"
  )

  echo "$(printf '%s\n' "${hints[@]}")"
else
  query="$@"

  [[ -n $query ]] || exit

  xdg-open "https://duckduckgo.com?q=$query" &>/dev/null &
fi
