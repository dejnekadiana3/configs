#!/bin/sh

get () {
  ls $XDG_CONFIG_HOME/dim/scripts/ | awk -F '.sh' '{ print $1 }'
}

run () {
  local script="$1"

  pexec dim $script
}

case "$1" in
  "get") get;;
  "run") run "$2";;
esac
