#!/bin/sh

get () {
  prefix=${PASSWORD_STORE_DIR}
  password_files=( "$prefix"/**/*.gpg )
  password_files=( "${password_files[@]#"$prefix"/}" )
  password_files=( "${password_files[@]%.gpg}" )

  printf '%s\n' "${password_files[@]}"
}

run () {
  local password="$1"

  pexec pass show -c "$password"
}

case "$1" in
  "get") get;;
  "run") run "$2";;
esac
