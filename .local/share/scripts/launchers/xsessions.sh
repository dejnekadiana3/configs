#!/bin/sh

get () {
  find /usr/share/xsessions/ -type f -printf "%f\n" | awk -F '.desktop' ' { print $1}'
}

run () {
  local xsession="$1"
  local execLine="$(grep '^Exec' /usr/share/xsessions/$xsession.desktop | tail -1)"
  local command="$(printf "%s" "$execLine" | sed 's/^Exec=//' | sed 's/%.//' | sed 's/^"//g' | sed 's/" *$//g')"

  # exec $command
  echo "This is not currently working"
}

case "$1" in
  "get") get;;
  "run") run "$2";;
esac
