#!/bin/sh

get () {
  ls /usr/share/applications | awk -F '.desktop' ' { print $1}'
}

run () {
  local app="$1"

  pexec gtk-launch "$app"
}

case "$1" in
  "get") get;;
  "run") run "$2";;
esac
