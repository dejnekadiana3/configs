#!/bin/sh

get () {
  compgen -c | sort -u
}

run () {
  local cmd="$1"
  local user_input="$2"

  if [[ "$user_input" == "$cmd"* ]]; then
    cmd="$user_input"
  fi

  pexec alacritty --hold -e bash -c "$cmd; bash"
}

run_detached () {
  local cmd="$1"
  local user_input="$2"

  if [[ "$user_input" == "$cmd"* ]]; then
    cmd="$user_input"
  fi

  pexec $cmd
}

case "$1" in
  "get") get;;
  "run") run "$2" "$3";;
  "run-detached") run_detached "$2" "$3";;
esac
