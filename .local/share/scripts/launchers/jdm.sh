#!/bin/sh

get () {
  sessions=$(jdm list-others | awk '{ print "switch "$1" "substr($2, 4) }')

  [[ -n "$sessions" ]] && printf '%s\n' "${sessions[@]}"
  printf '%s' "create"
}

run () {
  local command="$1"

  jdm $command
}

case "$1" in
  "get") get;;
  "run") run "$2";;
esac
