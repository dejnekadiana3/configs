#!/bin/sh

get () {
  ducks=(
    "!g"
    "!yt"
    "!gtuk"
  )

  printf "%s\n" "${ducks[@]}"
}

run () {
  local query="$1"

  pexec xdg-open "https://duckduckgo.com?q=$query"
}

case "$1" in
  "get") get;;
  "run") run "$2";;
esac
