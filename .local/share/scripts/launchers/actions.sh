#!/bin/sh

screenshot () { pexec flameshot gui -d 1000; }

lock () { pexec sh $XDG_DATA_HOME/scripts/i3-lock-with-blur.sh; }

i3_logout () { i3-msg exit; }
bspwm_logout () { bspc quit; }
awesomewm_logout () { awesome-client 'awesome.quit()'; }

jdm () { jdm $@; }

suspend () { systemctl suspend; }
lock_and_suspend () { lock && suspend; }

hibernate () { systemctl hibernate; }
lock_and_hibernate () { lock && hibernate; }

reboot () { systemctl reboot; }
shutdown () { systemctl poweroff -i; }

off_screen () { sleep 1 && xset dpms force off; }
lock_and_off_screen () { lock && off_screen; }

# action_commands='{
#   "screenshot": "sleep 1 && flameshot gui;",
#   "lock": "sh $XDG_DATA_HOME/scripts/i3-lock-with-blur.sh;",
#   "i3_logout": "i3-msg exit;",
#   "switch": "dim jdm;",
#   "lock_and_switch": "lock && switch;",
#   "suspend": "systemctl suspend;",
#   "lock_and_suspend": "lock && suspend;",
#   "hibernate": "systemctl hibernate;",
#   "lock_and_hibernate": "lock && hibernate;",
#   "reboot": "systemctl reboot;",
#   "shutdown": "sleep 1 && xset dpms force off;",
#   "off_screen": "systemctl poweroff -i;",
#   "lock_and_off_screen": "lock && off_screen;"
# }'

get () {
  actions=(
    "screenshot"
    "lock"
    "i3_logout"
    "bspwm_logout"
    "awesomewm_logout"
    "lock_and_switch"
    "suspend"
    "lock_and_suspend"
    "hibernate"
    "lock_and_hibernate"
    "reboot"
    "shutdown"
    "off_screen"
    "lock_and_off_screen"
  )

  printf "%s\n" "${actions[@]}"
  sh $XDG_DATA_HOME/scripts/launchers/jdm.sh get | awk '{ print "jdm "$0 }'
}

run_detached () {
  local action="$@"

  pexec $action
}

run () {
  local action="$@"

  $action
}

case "$1" in
  "get") get;;
  "run") run "$2";;
esac
