#!/bin/sh

sh $XDG_DATA_HOME/scripts/launchers/commands.sh get \
  | dim menu \
  | xargs -r -d '\n' sh $XDG_DATA_HOME/scripts/launchers/commands.sh run-detached
