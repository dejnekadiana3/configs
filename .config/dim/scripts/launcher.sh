#!/bin/sh

plugin_map='{
  "actions": {
    "scriptGet": ".local/share/scripts/launchers/actions.sh get",
    "scriptRun": ".local/share/scripts/launchers/actions.sh run"
  },
  "jdm": {
    "scriptGet": ".local/share/scripts/launchers/jdm.sh get",
    "scriptRun": ".local/share/scripts/launchers/jdm.sh run"
  },
  "app": {
    "scriptGet": ".local/share/scripts/launchers/applications.sh get",
    "scriptRun": ".local/share/scripts/launchers/applications.sh run"
  },
  "command": {
    "scriptGet": ".local/share/scripts/launchers/commands.sh get",
    "scriptRun": ".local/share/scripts/launchers/commands.sh run"
  },
  "command-detached": {
    "scriptGet": ".local/share/scripts/launchers/commands.sh get",
    "scriptRun": ".local/share/scripts/launchers/commands.sh run-detached"
  },
  "dim-scripts": {
    "scriptGet": ".local/share/scripts/launchers/dim-scripts.sh get",
    "scriptRun": ".local/share/scripts/launchers/dim-scripts.sh run"
  },
  "pass": {
    "scriptGet": ".local/share/scripts/launchers/pass.sh get",
    "scriptRun": ".local/share/scripts/launchers/pass.sh run"
  },
  "duckduckgo": {
    "scriptGet": ".local/share/scripts/launchers/duckduckgo.sh get",
    "scriptRun": ".local/share/scripts/launchers/duckduckgo.sh run"
  }
}'

launcher_cache=/tmp/$USER/dim-launcher
mkdir -p $launcher_cache

plugin_get () {
  local plugin_name="$1"
  local plugin_script="$(printf "$plugin_map" | jq -r ".\"$plugin_name\".scriptGet")"

  sh "$HOME/"$plugin_script | awk "{ print \$0\"[$plugin_name]\"}"
}

plugin_run () {
  local plugin_name="$1"
  local item="$(printf "%s" "$2" | sed "s/\[$plugin_name\]\$//")"
  local plugin_script="$(printf "$plugin_map" | jq -r ".\"$plugin_name\".scriptRun")"

  sh "$HOME/"$plugin_script "$item"
}

get_items () {
  printf "s \nc \n"
  plugin_get actions
  # plugin_get jdm
  plugin_get app
  # plugin_get command
  plugin_get command-detached
  plugin_get dim-scripts
  plugin_get pass
  plugin_get duckduckgo
}

get_cached_items () {
  local cache_file="$launcher_cache/items"
  local cache_file_tmp="$launcher_cache/items_tmp"

  if [ -f "$cache_file" ]; then
    cat "$cache_file"
  fi

  get_items > $cache_file_tmp

  local new_entries="$(cat $cache_file_tmp | cat $cache_file - | sort | uniq -u)"

  echo "$new_entries"

  cat "$cache_file_tmp" > $cache_file
}

get_hints () {
  help=(
    "s * - duckduckgo search"
    "c * - terminal command"
  )

  printf "%s\n" "${help[@]}"
}

menu_output="$(get_cached_items | dim menu "printf '%s' '$(get_hints)'")"
user_input="$(printf "%s" "$menu_output" | awk 'NR==1')"
items="$(printf "%s" "$menu_output" | awk 'NR!=1')"

case "$user_input" in
  "s "*) plugin_run duckduckgo "$(printf "%s" "$user_input" | sed 's/^s //')"; exit;;
  "c "*) plugin_run command "$(printf "%s" "$user_input" | sed 's/^c //"')"; exit;;
esac

printf "$items\n" | while read item; do
  case "$item" in
    *"[actions]") plugin_run actions "$item";;
    *"[jdm]") plugin_run jdm "$item";;
    *"[app]") plugin_run app "$item";;
    *"[command]") plugin_run command "$item";;
    *"[command-detached]") plugin_run command-detached "$item";;
    *"[dim-scripts]") plugin_run dim-scripts "$item";;
    *"[pass]") plugin_run pass "$item";;
    *"[duckduckgo]") plugin_run duckduckgo "$item";;
  esac
done
