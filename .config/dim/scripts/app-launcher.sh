#!/bin/sh

sh $XDG_DATA_HOME/scripts/launchers/applications.sh get \
  | dim menu 'echo "app launcher";' \
  | awk 'NR==2' \
  | xargs -r -d '\n' sh $XDG_DATA_HOME/scripts/launchers/applications.sh run
