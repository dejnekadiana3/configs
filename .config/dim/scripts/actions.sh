#!/bin/sh

sh $XDG_DATA_HOME/scripts/launchers/actions.sh get \
  | dim menu \
  | awk 'NR==2' \
  | xargs -r -d '\n' sh $XDG_DATA_HOME/scripts/launchers/actions.sh run
