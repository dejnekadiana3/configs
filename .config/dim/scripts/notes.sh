#!/bin/sh

notes_dir=~/Notes
mkdir -p "$notes_dir"

number_of_files="$(ls $notes_dir | wc -l)"

(( number_of_files != 1 )) \
  && file="$(ls $notes_dir | dim menu | awk 'NR==2')" \
  || file="$(ls $notes_dir)"

[[ -n "$file" ]] && dim edit last 0 "$notes_dir/$file" || dim edit last 0 "$notes_dir/main.md"

