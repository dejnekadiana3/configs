#!/bin/sh

menu_output="$(sh $XDG_DATA_HOME/scripts/launchers/duckduckgo.sh get | dim menu)"

user_input="$(echo "$menu_output" | awk 'NR==1')"
item="$(echo "$menu_output" | awk 'NR==2')"

query="$user_input"

for duck in "${ducks[@]}"; do
  if [[ "$item" == "$duck" ]] || [[ "$user_input" == "$duck" ]]; then
    query=$(echo "$duck " | dim edit 1 'last')
  fi
done

sh $XDG_DATA_HOME/scripts/launchers/applications.sh run "$query"
