#!/bin/sh

sh $XDG_DATA_HOME/scripts/launchers/dim-scripts.sh get \
  | dim menu \
  | awk 'NR==2' \
  | xargs -r -d '\n' sh $XDG_DATA_HOME/scripts/launchers/dim-scripts.sh run
