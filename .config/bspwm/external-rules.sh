#!/bin/sh

window_id="$1"
window_class="$2"
window_instance="$3"
consequences="$4"

# echo "$@" >> "$HOME/.cache/bspwm-external-rules.log"

current_node_presel="$(bspc query -T -n | jq '.presel')"
[[ "$current_node_presel" != "null" ]] && exit

# current_node="$(bspc query -T -n | jq '.id')"
# second_child_of_parent_node="$(bspc query -T -n @parent | jq '.secondChild.id')"
# if (( current_node == second_child_of_parent_node )); then
#   echo "node=@/1"
# fi

echo "node=@/1"
