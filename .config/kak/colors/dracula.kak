# Dracula theme for Kakoune
# A dark theme for Kakoune
# https://draculatheme.com
# https://kakoune.org

# Color palette
# https://github.com/dracula/dracula-theme#color-palette
declare-option str black 'rgb:282a36'
declare-option str gray 'rgb:cccccc'
declare-option str dimmed_gray 'rgb:777777'
declare-option str white 'rgb:f8f8f2'
declare-option str blue 'rgb:6272a4'
declare-option str cyan 'rgb:8be9fd'
declare-option str green 'rgb:50fa7b'
declare-option str orange 'rgb:ffb86c'
declare-option str pink 'rgb:ff79c6'
declare-option str purple 'rgb:bd93f9'
declare-option str red 'rgb:ff5555'
declare-option str yellow 'rgb:f1fa8c'

declare-option str background %opt{black}
declare-option str dimmed_background %opt{gray}
declare-option str foreground %opt{white}

# Reference
# https://github.com/mawww/kakoune/blob/master/colors/default.kak
# For code
set-face global value "%opt{purple}"
set-face global type "%opt{purple}+i"
set-face global variable "%opt{orange}"
set-face global module "%opt{red}+i"
set-face global function "%opt{green}"
set-face global string "%opt{yellow}"
set-face global keyword "%opt{pink}+i"
set-face global operator "%opt{cyan}"
set-face global attribute "%opt{orange}+i"
set-face global comment "%opt{blue}+i"
set-face global meta "%opt{red}"
set-face global builtin "%opt{pink}"

# For markup
set-face global title "%opt{red}"
set-face global header "%opt{orange}"
set-face global bold "%opt{pink}"
set-face global italic "%opt{purple}"
set-face global mono "%opt{green}"
set-face global block "%opt{cyan}"
set-face global link "%opt{green}"
set-face global bullet "%opt{green}"
set-face global list "%opt{white}"

# Builtin faces
# set-face global Default "%opt{white},%opt{black}"
set-face global PrimarySelection "%opt{black},%opt{pink}"
set-face global SecondarySelection "%opt{black},%opt{purple}"
set-face global PrimaryCursor "%opt{black},%opt{white}"
set-face global SecondaryCursor "%opt{black},%opt{orange}"
set-face global PrimaryCursorEol "%opt{black},%opt{white}"
set-face global SecondaryCursorEol "%opt{black},%opt{pink}"
set-face global LineNumbers "%opt{blue}"
set-face global LineNumberCursor "%opt{white}"
set-face global LineNumbersWrapped "%opt{orange}"
set-face global MenuForeground "%opt{purple}+b"
set-face global MenuBackground "%opt{purple}"
set-face global MenuInfo "%opt{purple}"
set-face global Information "%opt{white}"
set-face global Error "%opt{red},%opt{black}"
set-face global StatusLine "%opt{white}"
set-face global StatusLineMode "%opt{black},%opt{pink}+b"
set-face global StatusLineInfo "%opt{purple}"
set-face global StatusLineValue "%opt{orange},%opt{black}"
set-face global StatusCursor "%opt{white},%opt{white}"
set-face global Prompt "%opt{green},%opt{black}"
set-face global MatchingChar "%opt{cyan}"
set-face global Whitespace "%opt{dimmed_gray}"
set-face global WrapMarker Whitespace
# set-face global BufferPadding "%opt{gray},%opt{black}"
