hook global WinSetOption filetype=json %{
  set-option buffer formatcmd "fixjson"

  # remove-highlighter shared/json/string
  # add-highlighter shared/json/code/ regex '("\s*[^"]*\s*")\s*:' 1:variable
  # add-highlighter shared/json/code/ regex '(?<!\\)(\\\\)*(")' 2:string
  # add-highlighter shared/json/code/ regex '("(?!:).*?(?<!\\)(\\\\)*")\s*(?!:)' 1:string
  # add-highlighter shared/json/string region '"' '(?<!\\)(\\\\)*(?=:)"' fill string
}
