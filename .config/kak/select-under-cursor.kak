define-command -hidden -docstring "select a word under cursor, or add cursor on next occurrence of current selection" \
select-under-cursor %{
  try %{
    execute-keys "<a-k>\A.\z<ret>"
    execute-keys -save-regs '' "_<a-i>w*"
  } catch %{
    execute-keys -save-regs '' "_*N"
  } catch nop
}

define-command -hidden -docstring "select a word under cursor, or add cursor on previous occurrence of current selection" \
select-prev-under-cursor %{
  try %{
    execute-keys "<a-k>\A.\z<ret>"
    execute-keys -save-regs '' "_<a-i>w*"
  } catch %{
    execute-keys -save-regs '' "_*<a-N>"
  } catch nop
}

define-command -hidden -docstring "select a word under cursor and add cursor on all occurrence of current selection" \
select-all-under-cursor %{
  try %{
    execute-keys "<a-k>\A.\z<ret>"
    execute-keys -save-regs '/' "_<a-i>w*%%s<c-r>/<ret>"
  } catch %{
    execute-keys -save-regs '' "_*N%%s<c-r>/<ret>"
  } catch nop
}
