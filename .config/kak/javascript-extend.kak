hook global WinSetOption filetype=javascript %{
  set-option buffer formatcmd "prettier --stdin-filepath %val{buffile}"
  set-option buffer lintcmd "eslint --format /lib/node_modules/eslint-formatter-kakoune"

  add-highlighter shared/javascript/code/ regex (?<!\?)\s([\w_$]{1}[\w\d_$]*)\s*: 1:variable
  add-highlighter shared/javascript/code/ regex [^\w_$](?!if)(?!for)(?!while)([\w_$]{1}[\w\d_$]*)\s*\( 1:function
  add-highlighter shared/javascript/code/ regex (&&|\|\||\?) 1:operator
  add-highlighter shared/javascript/code/ regex \s(:)\s 1:operator
  add-highlighter shared/javascript/code/ regex \b(console|module|process|Map|Set|WeakMap|WeakSet|Proxy|Promise|Array|Boolean|Date|Function|Number|Object|RegExp|String|Symbol)\b 1:builtin
}
