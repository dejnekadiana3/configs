# General langmaps
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
declare-option str-list langmap_us_qwerty %{\|qQwWeErRtTyYuUiIoOpP[{]}aAsSdDfFgGhHjJkKlL;:'"zZxXcCvVbBnNmM,<.>}
declare-option str-list langmap_ua_qwerty %{ґҐйЙцЦуУкКеЕнНгГшШщЩзЗхХїЇфФіІвВаАпПрРоОлЛдДжЖєЄяЯчЧсСмМиИтТьЬбБюЮ}

# Dvorak langmaps
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
declare-option str-list langmap_us_dvorak %{`~#$%^[{]}\|pPyYfFgGcCrRlL=+aAoOeEuUiIdDhHtTnNsSqQjJkKxXbBmMwWvVzZ}
declare-option str-list langmap_ua_dvorak %{юЮєЄъЪшШщЩєЄпПіІфФгГцЦрРлЛчЧаАоОеЕуУиИдДхХтТнНсСяЯйЙкКьЬбБмМвВжЖзЗ}

# Macbook langmaps
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
declare-option str-list langmap_eu_qwerty_mac %{qQwWeErRtTyYuUiIoOpP[{]}aAsSdDfFgGhHjJkKlL;:'"\|zZxXcCvVbBnNmM,<.>}
declare-option str-list langmap_ua_qwerty_mac %{йЙцЦуУкКеЕнНгГшШщЩзЗхХїЇфФіІвВаАпПрРоОлЛдДжЖєЄґҐяЯчЧсСмМиИтТьЬбБюЮ}

declare-option str-list langmap_default
declare-option str-list langmap

define-command -docstring 'langmap-display-layout <langmap>: display <langmap> option value in info box formatted as keyboard layout. Accepts ''%opt{langmap_lang_map}'' or str-list formatted langmap as a parameter' \
  langmap %{ evaluate-commands %sh{
    langmap_default=$kak_opt_langmap_default
    langmap=$kak_opt_langmap

    for (( i=0; i<${#langmap}; i++ )); do
      # printf "%s %%\u266f %s -- %%\u00a2%s\u00a2 %%\u03b5%s\u03b5 \u266f\n" "evaluate-commands -client $kak_client" "map buffer normal" "${langmap:$i:1}" "${langmap_default:$i:1}" | kak -p $kak_session
      printf "%s %%{ %s %%♪%s♪ %%♪%s♪ }\n" "evaluate-commands -client $kak_client" "map buffer normal" "${langmap:$i:1}" "${langmap_default:$i:1}" | kak -p $kak_session
      # printf "%s %%{ %s %%♪%s♪ %%♪%s♪ }\n" "evaluate-commands -client $kak_client" "map buffer normal" "${langmap:$i:1}" "${langmap_default:$i:1}" >> /tmp/tmp2.kak
    done
  }}


