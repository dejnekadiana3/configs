declare-option -hidden str source %val{source}

define-command japanese-kanji-select %{
  execute-keys "di%sh{ tmux-exec ""cat '$(dirname $kak_opt_source)/bin/japanese-kanji.l' | fzf -q '$kak_selection' | awk -F '/' '{print \$2}'"" }<esc>"
}

define-command japanese-load-user-modes %{
  evaluate-commands %sh{ 
    node "$(dirname $kak_opt_source)/bin/japanese.js" | kak -p  $kak_session
  }
}

